<?php
/**
 * Created by PhpStorm.
 * User: DIMA_SFN
 * Date: 30.07.2020
 * Time: 14:45
 */

namespace common\helpers;


class DynamicModel extends \yii\base\DynamicModel {

    protected $_labels;

    public function setAttributeLabels($labels){
        $this->_labels = $labels;
    }

    public function getAttributeLabel($name){
        return $this->_labels[$name] ?? $name;
    }
}