<?php

/**
 * @var $model \yii\base\DynamicModel
 */

use yii\widgets\ActiveForm;

$formClientId = 'dynamic-form-client';
echo \yii\helpers\Html::tag('h3', $formClientId);
$formClient = ActiveForm::begin(['id' => $formClientId]);

echo $formClient->field($model, 'name')->textInput();

ActiveForm::end();


$formAjaxId = 'dynamic-form-ajax';
echo \yii\helpers\Html::tag('h3', $formAjaxId);
$formAjax = ActiveForm::begin([
    'id' => $formAjaxId,
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
]);

echo $formAjax->field($model, 'name'/*, ['enableAjaxValidation' => true] */)->textInput();

ActiveForm::end();
