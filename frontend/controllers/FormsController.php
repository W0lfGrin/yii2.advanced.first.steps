<?php
/**
 * Created by PhpStorm.
 * User: DIMA_SFN
 * Date: 29.07.2020
 * Time: 16:22
 */

namespace frontend\controllers;


use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;

class FormsController extends Controller
{

    public function actionDynamicModel()
    {
        $model = new \common\helpers\DynamicModel([
            'name', 'email', 'address'
        ]);
        $model->addRule(['name', 'email'], 'required')
            ->addRule(['email'], 'email')
            ->addRule('name', 'string', ['max' => 3]);

        $model->setAttributeLabels(['name' => 'Name', 'email' => 'EMAIL']);

        if($model->load(Yii::$app->request->post())){

            if (Yii::$app->request->isAjax){
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }

            return $this->redirect(['dynamic-mode']);
        }
        return $this->render('dynamic-model', compact('model'));
    }
}