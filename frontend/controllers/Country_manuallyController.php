<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 11.04.2018
 * Time: 11:40
 */

namespace frontend\controllers;


use yii\base\Controller;
use yii\data\Pagination;
use frontend\models\Country_manually;

class Country_manuallyController extends Controller
{
    public function actionIndex()
    {
        $query = Country_manually::find();

        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $countries = $query->orderBy('name')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('index', [
            'countries' => $countries,
            'pagination' => $pagination,
        ]);
    }

}