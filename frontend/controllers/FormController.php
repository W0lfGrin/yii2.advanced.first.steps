<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 09.04.2018
 * Time: 13:31
 */

namespace frontend\controllers;

use Yii;
use frontend\models\EntryForm;
//use app\model\EntryForm;
use yii\base\Controller;

class FormController extends Controller
{

    public function actionEntry()
    {
        $model = new EntryForm();

        if($model->load(Yii::$app->request->post()) && $model->validate())
        {
            // данные в $model удачно проверены
            // делаем что-то полезное с $model ...
            return $this->render('entry-confirm', ['model' => $model]);
        } else {
            // либо страница отображается первый раз, либо есть ошибка в данных
            return $this->render('entry', ['model' => $model]);
        }
    }
}