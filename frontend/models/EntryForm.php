<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 09.04.2018
 * Time: 16:50
 */

namespace frontend\models;

use yii\base\Model;

class EntryForm extends Model
{
    public $name;
    public $pass;
    public $email;

    public function rules()
    {
        return [
            [['name', 'pass', 'email'], 'required', 'message' => 'Поле обязательно для заполнения'],
            ['pass', 'string', 'min' => 6, 'tooShort' => 'Минимум 6 символов'],
            ['email','email']
        ];
    }
}